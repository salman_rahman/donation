<?php

use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\AdminController;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/phpinfo', function () {
    return phpinfo();
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::middleware('auth')->group(function () {
    Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('dashboard');

    // blog route Start
    Route::get('blog/list', [BlogController::class, 'index'])->name('list.blog');
    Route::get('blog/create', [BlogController::class, 'create'])->name('add.blog');
    Route::post('blog/store', [BlogController::class, 'store'])->name('store.blog');
    Route::get('blog/edit/{id}', [BlogController::class, 'edit'])->name('edit.blog');
    Route::post('blog/update/', [BlogController::class, 'update'])->name('update.blog');
    Route::get('blog/delete/{id}', [BlogController::class, 'delete'])->name('delete.blog');

    //Profile Controller Start
    Route::get('profile/view', [ProfileController::class, 'profileView'])->name('profile.view');
    Route::post('profile/update', [ProfileController::class, 'profileUpdate'])->name('profile.update');
});
