@extends('layouts.admin_master')
@section('admin_content')
    <div class="container-fluid dashboard-default-sec">
        <div class="row">

            <div class="container-fluid general-widget">
                <div class="row">
                    <div class="col-sm-6 col-xl-3 col-lg-6">
                        <div class="card o-hidden border-0">
                            <div class="bg-primary b-r-4 card-body">
                                <div class="media static-top-widget">
                                    <div class="align-self-center text-center"><svg xmlns="http://www.w3.org/2000/svg"
                                            width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" class="feather feather-database">
                                            <ellipse cx="12" cy="5" rx="9" ry="3"></ellipse>
                                            <path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path>
                                            <path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path>
                                        </svg></div>
                                    <div class="media-body"><span class="m-0">Earnings</span>
                                        <h4 class="mb-0 counter">6659</h4><svg xmlns="http://www.w3.org/2000/svg"
                                            width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" class="feather feather-database icon-bg">
                                            <ellipse cx="12" cy="5" rx="9" ry="3"></ellipse>
                                            <path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path>
                                            <path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xl-3 col-lg-6">
                        <div class="card o-hidden border-0">
                            <div class="bg-secondary b-r-4 card-body">
                                <div class="media static-top-widget">
                                    <div class="align-self-center text-center"><svg xmlns="http://www.w3.org/2000/svg"
                                            width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" class="feather feather-shopping-bag">
                                            <path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path>
                                            <line x1="3" y1="6" x2="21" y2="6"></line>
                                            <path d="M16 10a4 4 0 0 1-8 0"></path>
                                        </svg></div>
                                    <div class="media-body"><span class="m-0">Products</span>
                                        <h4 class="mb-0 counter">9856</h4><svg xmlns="http://www.w3.org/2000/svg"
                                            width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" class="feather feather-shopping-bag icon-bg">
                                            <path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path>
                                            <line x1="3" y1="6" x2="21" y2="6"></line>
                                            <path d="M16 10a4 4 0 0 1-8 0"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xl-3 col-lg-6">
                        <div class="card o-hidden border-0">
                            <div class="bg-primary b-r-4 card-body">
                                <div class="media static-top-widget">
                                    <div class="align-self-center text-center"><svg xmlns="http://www.w3.org/2000/svg"
                                            width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" class="feather feather-message-circle">
                                            <path
                                                d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z">
                                            </path>
                                        </svg></div>
                                    <div class="media-body"><span class="m-0">Messages</span>
                                        <h4 class="mb-0 counter">893</h4><svg xmlns="http://www.w3.org/2000/svg"
                                            width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" class="feather feather-message-circle icon-bg">
                                            <path
                                                d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z">
                                            </path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xl-3 col-lg-6">
                        <div class="card o-hidden border-0">
                            <div class="bg-primary b-r-4 card-body">
                                <div class="media static-top-widget">
                                    <div class="align-self-center text-center"><svg xmlns="http://www.w3.org/2000/svg"
                                            width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" class="feather feather-user-plus">
                                            <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                            <circle cx="8.5" cy="7" r="4"></circle>
                                            <line x1="20" y1="8" x2="20" y2="14"></line>
                                            <line x1="23" y1="11" x2="17" y2="11"></line>
                                        </svg></div>
                                    <div class="media-body"><span class="m-0">New Use</span>
                                        <h4 class="mb-0 counter">4531</h4><svg xmlns="http://www.w3.org/2000/svg"
                                            width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round" class="feather feather-user-plus icon-bg">
                                            <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                            <circle cx="8.5" cy="7" r="4"></circle>
                                            <line x1="20" y1="8" x2="20" y2="14"></line>
                                            <line x1="23" y1="11" x2="17" y2="11"></line>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 xl-100 box-col-12">
                        <div class="card">
                            <div class="cal-date-widget card-body">
                                <div class="row">
                                    <div class="col-xl-6 col-xs-12 col-md-6 col-sm-6">
                                        <div class="cal-info text-center">
                                            <div>
                                                <h2>24</h2>
                                                <div class="d-inline-block"><span class="b-r-dark pe-3">March</span><span
                                                        class="ps-3">2018</span></div>
                                                <p class="f-16">There is no minimum donation, any sum is appreciated</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-xs-12 col-md-6 col-sm-6">
                                        <div class="cal-datepicker">
                                            <div class="datepicker-here float-sm-end" data-language="en">
                                                <div class="datepicker-inline">
                                                    <div class="datepicker"><i class="datepicker--pointer"></i>
                                                        <nav class="datepicker--nav">
                                                            <div class="datepicker--nav-action" data-action="prev"><svg>
                                                                    <path d="M 17,12 l -5,5 l 5,5"></path>
                                                                </svg></div>
                                                            <div class="datepicker--nav-title">May, <i> 2023 </i></div>
                                                            <div class="datepicker--nav-action" data-action="next"><svg>
                                                                    <path d="M 14,12 l 5,5 l -5,5"></path>
                                                                </svg></div>
                                                        </nav>
                                                        <div class="datepicker--content">
                                                            <div class="datepicker--days datepicker--body active">
                                                                <div class="datepicker--days-names">
                                                                    <div class="datepicker--day-name -weekend-">S</div>
                                                                    <div class="datepicker--day-name">M</div>
                                                                    <div class="datepicker--day-name">T</div>
                                                                    <div class="datepicker--day-name">W</div>
                                                                    <div class="datepicker--day-name">T</div>
                                                                    <div class="datepicker--day-name">F</div>
                                                                    <div class="datepicker--day-name -weekend-">S</div>
                                                                </div>
                                                                <div class="datepicker--cells datepicker--cells-days">
                                                                    <div class="datepicker--cell datepicker--cell-day -weekend- -other-month-"
                                                                        data-date="30" data-month="3" data-year="2023">30
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="1" data-month="4" data-year="2023">1
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="2" data-month="4" data-year="2023">2
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="3" data-month="4" data-year="2023">3
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="4" data-month="4" data-year="2023">4
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="5" data-month="4" data-year="2023">5
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -weekend-"
                                                                        data-date="6" data-month="4" data-year="2023">6
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -weekend-"
                                                                        data-date="7" data-month="4" data-year="2023">7
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="8" data-month="4" data-year="2023">8
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="9" data-month="4" data-year="2023">9
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="10" data-month="4" data-year="2023">10
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="11" data-month="4" data-year="2023">11
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="12" data-month="4" data-year="2023">12
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -weekend-"
                                                                        data-date="13" data-month="4" data-year="2023">13
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -weekend-"
                                                                        data-date="14" data-month="4" data-year="2023">14
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="15" data-month="4" data-year="2023">15
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="16" data-month="4" data-year="2023">16
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -current-"
                                                                        data-date="17" data-month="4" data-year="2023">17
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="18" data-month="4" data-year="2023">18
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="19" data-month="4" data-year="2023">19
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -weekend-"
                                                                        data-date="20" data-month="4" data-year="2023">20
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -weekend-"
                                                                        data-date="21" data-month="4" data-year="2023">21
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="22" data-month="4" data-year="2023">22
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="23" data-month="4" data-year="2023">23
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="24" data-month="4" data-year="2023">24
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="25" data-month="4" data-year="2023">25
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="26" data-month="4" data-year="2023">26
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -weekend-"
                                                                        data-date="27" data-month="4" data-year="2023">27
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -weekend-"
                                                                        data-date="28" data-month="4" data-year="2023">28
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="29" data-month="4" data-year="2023">29
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="30" data-month="4" data-year="2023">30
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day"
                                                                        data-date="31" data-month="4" data-year="2023">31
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -other-month-"
                                                                        data-date="1" data-month="5" data-year="2023">1
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -other-month-"
                                                                        data-date="2" data-month="5" data-year="2023">2
                                                                    </div>
                                                                    <div class="datepicker--cell datepicker--cell-day -weekend- -other-month-"
                                                                        data-date="3" data-month="5" data-year="2023">3
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endsection
