@extends('layouts.admin_master')
@section('admin_content')
    <div class="container-fluid">
        <div class="edit-profile">
            <div class="row">
                <div class="col-xl-4">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4 class="card-title mb-0">My Profile</h4>
                            <div class="card-options"><a class="card-options-collapse" href="#"
                                    data-bs-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a
                                    class="card-options-remove" href="#" data-bs-toggle="card-remove"><i
                                        class="fe fe-x"></i></a></div>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="row mb-2">
                                    <div class="profile-title">
                                        <div class="media">

                                            <img class="rounded-circle" width="100" height="100" alt="Profile Image"
                                                src="{{ !empty($adminData->image) ? url('upload/profile/' . $adminData->image) : url('upload/no_image.jpg') }}">
                                            {{-- src="{{ asset('upload/profile/' . $adminData->image) }}"> --}}
                                            <div class="media-body">
                                                <h3 class="mb-1 f-20 txt-primary">{{ $adminData->name }}</h3>
                                                <p class="f-12">{{ $adminData->email }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <form class="card" method="post" action="{{ route('profile.update') }}"
                        enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="old_image" value="{{ $adminData->image }}">
                        <div class="card-header pb-0">
                            <h4 class="card-title mb-0">Update Profile</h4>
                            <div class="card-options">
                                <a class="card-options-collapse" href="#" data-bs-toggle="card-collapse"><i
                                        class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#"
                                    data-bs-toggle="card-remove"><i class="fe fe-x"></i>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="form-label">Name</label>
                                        <input class="form-control" name="name" type="text"
                                            value="{{ $adminData->name }}">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="form-label">Email address</label>
                                        <input class="form-control" name="email" type="email"
                                            value="{{ $adminData->email }}">
                                    </div>
                                </div>
                                {{-- <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="form-label">Password</label>
                                        <input class="form-control" type="password" value="password">
                                    </div>
                                </div> --}}

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label pt-0">Profile Image<span
                                                class="font-danger">*</span></label>
                                        <input type="file" class="form-control" name="image" id="image">
                                    </div>
                                    <img id="showImage"
                                        src="{{ !empty($adminData->image) ? url('upload/profile/' . $adminData->image) : url('upload/no_image.jpg') }}"
                                        alt="blog" width="100" height="100"
                                        style="border-radius: 50%;border:1px solid gray">
                                </div>

                            </div>
                        </div>
                        <div class="card-footer text-end">
                            <button class="btn btn-primary" type="submit">Update Profile</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#image').change(function(e) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#showImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);

            });
        });
    </script>
@endsection
