@extends('layouts.admin_master')
@section('admin_content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-sm-9">
                    <h3>Blog Create</h3>

                </div>
                <div class="col-sm-3">
                    <a href="{{ route('list.blog') }}" class="btn btn-success ml">Blog List</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="form theme-form">
                        <form action="{{ route('store.blog') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label>Blog Title</label>
                                        <input class="form-control" name="blog_title" type="text"
                                            placeholder="Project name *">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label>Enter Blog Description</label>
                                        <textarea id="editor1" name="blog_description" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="col-form-label pt-0">Upload Blog Image<span
                                                class="font-danger">*</span></label>
                                        <input type="file" class="form-control" name="blog_image" id="image">
                                    </div>
                                    <img id="showImage" src="{{ url('upload/no_image.jpg') }}" alt="blog" width="100"
                                        height="100">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label>Blog Video Link</label>
                                        <input class="form-control" name="blog_video" type="text"
                                            placeholder="Blog video link">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="text-end">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#image').change(function(e) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#showImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);

            });
        });
    </script>
@endsection
