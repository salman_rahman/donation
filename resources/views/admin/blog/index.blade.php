@extends('layouts.admin_master')

@section('admin_content')
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h5>All Blog List</h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="display" id="basic-1">
                        <thead>
                            <tr>
                                <th>SL</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($blogs as $key => $blog)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>
                                        <img src="{{ asset($blog->blog_image) }}" alt="category_image" width="70"
                                            width="70">
                                    </td>
                                    <td>{{ $blog->blog_title }}</td>
                                    <td>{!! substr($blog->blog_description, 0, 100) !!}</td>

                                    <td>{{ $blog->created_at }}</td>
                                    <td>
                                        {{-- <a href="" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a> --}}
                                        <a href="{{ route('edit.blog', $blog->id) }}" class="btn btn-warning btn-sm"><i
                                                class="fa fa-pencil"></i></a>
                                        <a href="{{ route('delete.blog', $blog->id) }}" id="delete"
                                            class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
