<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Brian2694\Toastr\Facades\Toastr;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function profileView()
    {
        $id = Auth::user()->id;
        $adminData = User::findOrFail($id);
        return view('admin.profile.profile_view', compact('adminData'));
    }

    public function profileUpdate(Request $request)
    {
        $old_img = $request->old_image;
        $id = Auth::user()->id;

        // if ($request->file('image')) {
        //     $image = $request->file('image');
        //     $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
        //     Image::make($image)->resize(120, 120)->save('upload/profile/' . $name_gen);
        //     $data['image'] = $name_gen;
        //     if (file_exists($old_img)) {
        //         unlink($old_img);
        //     }
        // }

        $data = User::findOrFail($id);
        $data->name = $request->name;
        $data->email = $request->email;


        if ($request->file('image')) {
            $file = $request->file('image');

            $filename = date('YmdHi') . $file->getClientOriginalName();
            $file->move(public_path('upload/profile'), $filename);
            $data['image'] = $filename;
            if (file_exists($old_img)) {
                unlink($old_img);
            }
        }

        $data->save();
        Toastr::success('Profile Successfully Updated :)', 'Success');
        return redirect()->back();
    }
}
