<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::all();
        return view('admin.blog.index', compact('blogs'));
    }

    public function create()
    {
        return view('admin.blog.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'blog_title' => 'required',
            'blog_description' => 'required',
            'blog_image' => 'required',

        ]);

        $image = $request->file('blog_image');
        $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
        Image::make($image)->resize(120, 120)->save('upload/blog/' . $name_gen);
        $save_url = 'upload/blog/' . $name_gen;

        Blog::insert([
            'blog_title' => $request->blog_title,
            'blog_slug' => strtolower(str_replace(' ', '-', $request->blog_title)),
            'blog_description' => $request->blog_description,
            'blog_image' => $save_url,
            'blog_video' => $request->blog_video,
            'created_at' => Carbon::now(),

        ]);
        Toastr::success('Blog Successfully Saved :)', 'Success');
        return redirect()->route('list.blog');
    }

    public function edit($id)
    {
        $blogs = Blog::findOrFail($id);
        return view('admin.blog.edit', compact('blogs'));
    }

    public function update(Request $request)
    {
        $blog_id = $request->id;
        $old_img = $request->old_image;

        if ($request->file('blog_image')) {
            $image = $request->file('blog_image');
            $name_gen = hexdec(uniqid()) . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(120, 120)->save('upload/blog/' . $name_gen);
            $save_url = 'upload/blog/' . $name_gen;
            if (file_exists($old_img)) {
                unlink($old_img);
            }
        }

        Blog::findOrFail($blog_id)->update([
            'blog_title' => $request->blog_title,
            'blog_slug' => strtolower(str_replace(' ', '-', $request->blog_title)),
            'blog_description' => $request->blog_description,
            'blog_image' => $save_url,
            'blog_video' => $request->blog_video,
            'created_at' => Carbon::now(),

        ]);

        Toastr::success('Blog Successfully Updated :)', 'Success');
        return redirect()->route('list.blog');
    }

    public function delete($id)
    {
        $blog = Blog::findOrFail($id);
        $img = $blog->blog_image;
        unlink($img);
        Blog::findOrFail($id)->delete();
        Toastr::success('Blog Successfully Deleted :)', 'Success');
        return redirect()->back();
    }
}
